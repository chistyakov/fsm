package com.alexchistyakov.fsm6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FSM<A extends Enum<A>, Z, W> {

    // use Map to don't allow create several similar states
    private final Map<A, State<A, Z, W>> states = new HashMap<A, State<A, Z, W>>();
    private State<A, Z, W> currentState;

    public FSM(final State<A, Z, W>... statesArray) {
        for (final State<A, Z, W> state : statesArray) {
            addState(state);
        }
    }

    public State<A, Z, W> getCurrentState() {
        return currentState;
    }

    protected void setCurrentState(final State<A, Z, W> currentState) {
        this.currentState = currentState;
    }

    protected void setCurrentState(final A alphabetState) {
        setCurrentState(getStateByAlphabet(alphabetState));
    }

    protected void addState(final State<A, Z, W> state) {
        state.setFsm(this);
        // set the state as current in case current state is null
        if (getCurrentState() == null)
            setCurrentState(state);
        states.put(state.getAlphabetState(), state);
    }

    public W handleInput(final Z inp) {
        return getCurrentState().handleInput(inp);
    }

    public State<A, Z, W> getStateByAlphabet(final A alphabetState) {
        if (!states.containsKey(alphabetState))
            throw new FSMException("error trying to get unconfigured state");
        return states.get(alphabetState);
    }

    @Override
    public String toString() {
        return getCurrentState().toString();
    }

    public List<W> run(List<Z> lInp) {
        List<W> lOut = null;
        if (!lInp.isEmpty()) {
            lOut = new ArrayList<W>(lInp.size());
        }
        for (Z z : lInp) {
            lOut.add(handleInput(z));
        }
        return lOut;
    }
}