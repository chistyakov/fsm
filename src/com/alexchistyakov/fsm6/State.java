package com.alexchistyakov.fsm6;

public abstract class State<A extends Enum<A>, Z, W> {

    private A alphabetState;
    private FSM<A, Z, W> fsm;

    public State(final A alphabetState) {
        this.alphabetState = alphabetState;
    }

    public A getAlphabetState() {
        return alphabetState;
    }

    public void setNextState(final A nextStateFromAphabet) {
        getFsm().setCurrentState(nextStateFromAphabet);
    }

    public FSM<A, Z, W> getFsm() {
        return fsm;
    }

    protected void setFsm(final FSM<A, Z, W> fsm) {
        this.fsm = fsm;
    }

    abstract public W handleInput(Z inp);

    @Override
    public String toString() {
        return alphabetState.name() + "[" + alphabetState.ordinal() + "]";
    }
}

