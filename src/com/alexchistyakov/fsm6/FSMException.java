package com.alexchistyakov.fsm6;

/**
 * Created with IntelliJ IDEA.
 * User: teamdevelopment
 * Date: 02.04.13
 * Time: 19:50
 * To change this template use File | Settings | File Templates.
 */
public class FSMException extends RuntimeException {
    private static final long serialVersionUID = -7668938756337039216L;

    public FSMException(String message) {
        super(message);
    }
}

