@echo off

rem echo START
rem check arguments
if "%1"=="" goto noArg1
set packagename=%1
rem replace dots with slash
set pathPackage=%packagename:.=/%
set srcFilePath=./src/%pathPackage%/*.java
set binFolder=./bin/%pathPackage%
if not exist %srcFilePath% goto noFile

if not exist ./bin mkdir bin
rem compiling
echo compiling %srcFilePath% ...
javac -sourcepath ./src -d ./bin %srcFilePath%
IF %ERRORLEVEL% NEQ 0 goto :notCompiled
echo COMPILED SUCCESSFULLY

if "%2"=="" goto :EOF
if not exist %binFolder%/%~n2.class goto :noMainFile
echo START %packagename%.%~n2 ...
java -classpath ./bin %packagename%.%~n2
goto :EOF

:noArg1
echo Please determine the package name
goto :EOF

:noFile
echo The file %srcFilePath% not found
echo Check the name of file or the name of package
goto :EOF

:notCompiled
echo COMPILING FAILED
goto :EOF

:noMainFile
echo The file with main function for starting project not found
goto :EOF 