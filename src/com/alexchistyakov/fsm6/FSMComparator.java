package com.alexchistyakov.fsm6;

public class FSMComparator<STATES_FIRST extends Enum<STATES_FIRST>, STATES_SECOND extends Enum<STATES_SECOND>, X, Y> {
    private FSM<STATES_FIRST, X, Y> fsmFirst;
    private FSM<STATES_SECOND, X, Y> fsmSecond;
    private STATES_FIRST[] statesFirstArray;
    private STATES_SECOND[] statesSecondArray;

    public FSMComparator(FSM<STATES_FIRST, X, Y> fsmFirst,
                         FSM<STATES_SECOND, X, Y> fsmSecond,
                         Class<STATES_FIRST> statesFirstElementType,
                         Class<STATES_SECOND> statesSecondElementType) {
        this.fsmFirst = fsmFirst;
        this.fsmSecond = fsmSecond;
        this.statesFirstArray = statesFirstElementType.getEnumConstants();
        this.statesSecondArray = statesSecondElementType.getEnumConstants();
    }

    protected FSM<STATES_FIRST, X, Y> getFSMFirst() {
        return fsmFirst;
    }

    protected FSM<STATES_SECOND, X, Y> getFSMSecond() {
        return fsmSecond;
    }

    protected STATES_FIRST[] getStatesFirstArray() {
        return statesFirstArray;
    }

    protected STATES_SECOND[] getStatesSecondArray() {
        return statesSecondArray;
    }
}
