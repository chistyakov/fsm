package com.alexchistyakov.fsm6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FSMEnumComparator<STATES_FIRST extends Enum<STATES_FIRST>,
        STATES_SECOND extends Enum<STATES_SECOND>,
        Z extends Enum<Z>, W> extends FSMComparator<STATES_FIRST, STATES_SECOND, Z, W> {

    private Z[] zArr;

    public FSMEnumComparator(FSM<STATES_FIRST, Z, W> fsmFirst,
                             FSM<STATES_SECOND, Z, W> fsmSecond,
                             Class<STATES_FIRST> statesFirstElementType,
                             Class<STATES_SECOND> statesSecondElementType,
                             Class<Z> inputsElementType) {
        super(fsmFirst, fsmSecond, statesFirstElementType, statesSecondElementType);
        this.zArr = inputsElementType.getEnumConstants();
    }

    public boolean bruteForceCompare() {
        int inpCount = zArr.length;
        int statesCount = getLargerStatesSize();
        for (int counter = 0; counter < Math.pow(inpCount, statesCount); counter++) {
            // we have to use ArrayList instead of the array Z[] because of generics
            // @see TIJ 4 ed. p. 475
            List<Z> lInp = new ArrayList<Z>(statesCount);
            for (int k = 0; k < statesCount; k++)
                lInp.add(zArr[0]);
            int currentCounter = counter;

            for (int j = 0; j < statesCount; j++) {
                int reminder = currentCounter % inpCount;
                currentCounter = currentCounter / inpCount;
                lInp.set(j, zArr[reminder]);
            }

            if (!checkWord(lInp))
                return false;
        }
        return true;
    }

    private boolean checkWord(List<Z> lInp) {
        System.out.println("input: " + lInp);
        List<W> lOutFirst = getFSMFirst().run(lInp);
        List<W> lOutSecond = getFSMSecond().run(lInp);
        System.out.println("first output: " + lOutFirst);
        System.out.println("second output: " + lOutSecond);
        return lOutFirst.equals(lOutSecond);
    }

    private int getLargerStatesSize() {
        int firstLength = getStatesFirstArray().length;
        int secondLength = getStatesSecondArray().length;
        return firstLength > secondLength ? firstLength : secondLength;
    }

    public boolean smartCompare() {
        STATES_FIRST[] statesFirstArray = getStatesFirstArray();
        STATES_SECOND[] statesSecondArray = getStatesSecondArray();
        FSM<STATES_FIRST, Z, W> fsmFirst = getFSMFirst();
        FSM<STATES_SECOND, Z, W> fsmSecond = getFSMSecond();

        // the map lUndistStates contains all undistinguished (неотличимые) pairs of states.
        // every item in lUndistStates contains the pair of states and the nested map of next states
        // every item in nested map contains the condition (input) to go to the next pair of states
        // and the pair of next states
        Map<Pair<STATES_FIRST, STATES_SECOND>, Map<Z, Pair<STATES_FIRST, STATES_SECOND>>> lUndistStates =
                new HashMap<Pair<STATES_FIRST, STATES_SECOND>, Map<Z, Pair<STATES_FIRST, STATES_SECOND>>>();
        // loop through all possible pairs of states
        for (int iFirst = 0; iFirst < statesFirstArray.length; iFirst++) {
            for (int iSecond = 0; iSecond < statesSecondArray.length; iSecond++) {
                Map<Z, Pair<STATES_FIRST, STATES_SECOND>> mapPossibleNextStates =
                        new HashMap<Z, Pair<STATES_FIRST, STATES_SECOND>>();
                // the flag unDistingFlag is set in true in case all outputs are undistinguished for all inputs
                boolean unDistingFlag = true;
                // loop through all possible inputs
                for (int ix = 0; ix < zArr.length; ix++) {
                    fsmFirst.setCurrentState(statesFirstArray[iFirst]);
                    fsmSecond.setCurrentState(statesSecondArray[iSecond]);
                    if (fsmFirst.handleInput(zArr[ix]).equals(fsmSecond.handleInput(zArr[ix]))) {
                        Pair<STATES_FIRST, STATES_SECOND> nextPair = new Pair<STATES_FIRST, STATES_SECOND>(
                                fsmFirst.getCurrentState().getAlphabetState(),
                                fsmSecond.getCurrentState().getAlphabetState());
                        mapPossibleNextStates.put(zArr[ix], nextPair);
                    } else {
                        // set the flag into the false in case the outputs are undistinguished
                        // and go to the next pair
                        unDistingFlag = false;
                        break;
                    }
                }
                // put the current pair of states into the map
                // in case all outputs are undistinguished for all inputs
                if (unDistingFlag) {
                    Pair<STATES_FIRST, STATES_SECOND> p =
                            Pair.create(statesFirstArray[iFirst], statesSecondArray[iSecond]);
                    lUndistStates.put(p, mapPossibleNextStates);
                }
            }
        }
        System.out.println(lUndistStates);

        return areAllStatesPairsCompatible(lUndistStates);
    }

    private boolean areAllStatesPairsCompatible(
            Map<Pair<STATES_FIRST, STATES_SECOND>, Map<Z, Pair<STATES_FIRST, STATES_SECOND>>> lUndistStates) {
        // the undistinguished state's pair is compatible if all possible next pairs of states are undistinguished
        if (lUndistStates.isEmpty())
            return false;
        // for every state's pair check all possible next state's pair
        // return false in case some next state's pair is undistinguishable (неотличимая)
        // the state's pair is undistinguishable if it's not contained in lUndistStates
        for (Pair<STATES_FIRST, STATES_SECOND> curState : lUndistStates.keySet()) {
            Map<Z, Pair<STATES_FIRST, STATES_SECOND>> mapPossibleNextStates = lUndistStates.get(curState);
            for (Z inp : mapPossibleNextStates.keySet()) {
                Pair<STATES_FIRST, STATES_SECOND> nextPair = mapPossibleNextStates.get(inp);
                if (!lUndistStates.containsKey(nextPair)) {
                    return false;
                }
            }
        }
        // return true in case all state's pair are compatible
        return true;
    }

}
