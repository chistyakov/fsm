package com.alexchistyakov.fsm6;

public class TestFSM6 {

    private enum States1 {
        A1, A2, A3;
    }

    private enum States2 {
        A1, A2;
    }

    private enum States3 {
        A1, A2;
    }

    // input alphabet
    private enum Z {
        O, I;
    }

    // output alphabet
    private enum W {
        O, I;
    }

    public static void main(String... args) {

        FSM<States1, Z, W> fsmEnum1 = createFSMEnum1();
        FSM<States2, Z, W> fsmEnum2 = createFSMEnum2();
        FSM<States3, Z, W> fsmEnum3 = createFSMEnum3();

        FSMEnumComparator<States1, States2, Z, W> comparator =
                new FSMEnumComparator<States1, States2, Z, W>(
                        fsmEnum1, fsmEnum2, States1.class, States2.class, Z.class);

        System.out.println("Test the first comparator");
        System.out.println("are fsmEnum1 and fsmEnum2 equivalent?");
        System.out.println("bruteforce compare:");
        System.out.println(comparator.bruteForceCompare());
        System.out.println("smarter compare:");
        System.out.println(comparator.smartCompare());
        System.out.println();

        FSMEnumComparator<States1, States3, Z, W> comparator2 =
                new FSMEnumComparator<States1, States3, Z, W>(
                        fsmEnum1, fsmEnum3, States1.class, States3.class, Z.class);
        System.out.println("Test the second comparator");
        System.out.println("are fsmEnum1 and fsmEnum3 equivalent?");
        System.out.println("bruteforce compare:");
        System.out.println(comparator2.bruteForceCompare());
        System.out.println("smarter compare:");
        System.out.println(comparator2.smartCompare());

        // We can create the FSM with Object objects for inputs and outputs
        // for example, Integer is used:
        FSM<States1, Integer, Integer> fsmInteger = createIntegerFSM();
    }

    private static FSM<States1, Z, W> createFSMEnum1() {
        return new FSM<States1, Z, W>(

                new State<States1, Z, W>(States1.A1) {
                    public W handleInput(Z inp) {
                        switch (inp) {
                            case O:
                                setNextState(States1.A2);
                                return W.O;
                            case I:
                                setNextState(States1.A3);
                                return W.I;
                            default:
                                return null;
                        }
                    }
                },
                new State<States1, Z, W>(States1.A2) {
                    public W handleInput(Z inp) {
                        switch (inp) {
                            case O:
                                setNextState(States1.A3);
                                return W.I;
                            case I:
                                setNextState(States1.A1);
                                return W.O;
                            default:
                                return null;
                        }
                    }
                },
                new State<States1, Z, W>(States1.A3) {
                    public W handleInput(Z inp) {
                        switch (inp) {
                            case O:
                                setNextState(States1.A2);
                                return W.O;
                            case I:
                                setNextState(States1.A1);
                                return W.I;
                            default:
                                return null;
                        }
                    }
                }
        );
    }

    private static FSM<States2, Z, W> createFSMEnum2() {
        return new FSM<States2, Z, W>(
                new State<States2, Z, W>(States2.A1) {
                    @Override
                    public W handleInput(Z inp) {
                        switch (inp) {
                            case O:
                                setNextState(States2.A2);
                                return W.O;
                            case I:
                                return W.I;
                            default:
                                return null;
                        }
                    }
                },
                new State<States2, Z, W>(States2.A2) {
                    @Override
                    public W handleInput(Z inp) {
                        switch (inp) {
                            case O:
                                setNextState(States2.A1);
                                return W.I;
                            case I:
                                setNextState(States2.A1);
                                return W.O;
                            default:
                                return null;
                        }
                    }
                }
        );
    }

    // this fsm particularly equals the second fsm, but one output value
    private static FSM<States3, Z, W> createFSMEnum3() {
        return new FSM<States3, Z, W>(
                new State<States3, Z, W>(States3.A1) {
                    @Override
                    public W handleInput(Z inp) {
                        switch (inp) {
                            case O:
                                setNextState(States3.A2);
                                // change W.I to W.O
                                return W.I;
                            case I:
                                return W.I;
                            default:
                                return null;
                        }
                    }
                },
                new State<States3, Z, W>(States3.A2) {
                    @Override
                    public W handleInput(Z inp) {
                        switch (inp) {
                            case O:
                                setNextState(States3.A1);
                                return W.I;
                            case I:
                                setNextState(States3.A1);
                                return W.O;
                            default:
                                return null;
                        }
                    }
                }
        );
    }

    private static FSM<States1, Integer, Integer> createIntegerFSM() {
        return new FSM<States1, Integer, Integer>(

                new State<States1, Integer, Integer>(States1.A1) {
                    public Integer handleInput(Integer inp) {
                        if (inp.equals(1)) {
                            setNextState(States1.A2);
                            return 1;
                        } else if (inp.equals(0)) {
                            setNextState(States1.A3);
                            return 0;
                        } else
                            return null;
                    }
                },
                new State<States1, Integer, Integer>(States1.A2) {
                    public Integer handleInput(Integer inp) {
                        if (inp.equals(1)) {
                            setNextState(States1.A3);
                            return 0;
                        } else if (inp.equals(0)) {
                            setNextState(States1.A1);
                            return 1;
                        } else
                            return null;
                    }
                },
                new State<States1, Integer, Integer>(States1.A3) {
                    public Integer handleInput(Integer inp) {
                        if (inp.equals(1)) {
                            setNextState(States1.A2);
                            return 1;
                        } else if (inp.equals(0)) {
                            setNextState(States1.A1);
                            return 0;
                        } else
                            return null;
                    }
                }
        );
    }
}
